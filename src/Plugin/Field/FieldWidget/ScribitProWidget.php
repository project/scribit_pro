<?php

namespace Drupal\scribit_pro\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\scribit_pro\HelperService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'scribit_pro_widget' widget.
 *
 * @FieldWidget(
 *   id = "scribit_pro_widget",
 *   module = "scribit_pro",
 *   label = @Translation("Scribit Pro oEmbed URL"),
 *   field_types = {
 *     "string",
 *   },
 * )
 */
class ScribitProWidget extends StringTextfieldWidget {

  /**
   * Scribit Pro helper service.
   *
   * @var \Drupal\scribit_pro\HelperService
   */
  protected $helper;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\scribit_pro\HelperService $helper
   *   Scribit Pro helper service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, HelperService $helper) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->helper = $helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('scribit_pro.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $element['scribit_pro'] = [
      '#tree' => TRUE,
      '#type' => 'fieldset',
      '#title' => $this->t('Scribit Pro'),
      '#collapsible' => FALSE,
      '#required' => FALSE,
      '#attributes' => [
        'data-scribit-pro-container' => 1,
      ],
    ];

    $element['scribit_pro']['#attached']['library'][] = 'scribit_pro/field_widget';

    if (isset($element['value']['#default_value']) && $project = $this->helper->getProjectByYouTubeUrl($element['value']['#default_value'])) {
      $element['scribit_pro']['video_exists_message'] = [
        '#type' => 'item',
        '#markup' => $this->t('This video has already been submitted to Scribit Pro'),
      ];
    }
    else {
      $element['scribit_pro']['enable'] = [
        '#type' => 'checkbox',
        '#tree' => TRUE,
        '#title' => $this->t('Submit video to Scribit Pro'),
        '#attributes' => [
          'data-scribit-pro-enable' => 1,
        ],
        '#required' => FALSE,
      ];

      $element['scribit_pro']['voice'] = [
        '#type' => 'radios',
        '#tree' => TRUE,
        '#title' => $this->t('Voice'),
        '#options' => [
          'male' => $this->t('Male'),
          'female' => $this->t('Female'),
        ],
        '#attributes' => [
          'data-scribit-pro-field' => 'voice',
        ],
        '#states' => [
          'visible' => [
            ':input[data-scribit-pro-enable="1"]' => ['checked' => TRUE],
          ],
        ],
        '#element_validate' => [
          [$this, 'settingsFormRequiredValidate'],
        ],
        '#required' => FALSE,
      ];

      $element['scribit_pro']['language'] = [
        '#type' => 'radios',
        '#tree' => TRUE,
        '#title' => $this->t('Language'),
        '#options' => $this->helper->getLanguages(),
        '#attributes' => [
          'data-scribit-pro-field' => 'language',
        ],
        '#states' => [
          'visible' => [
            ':input[data-scribit-pro-enable="1"]' => ['checked' => TRUE],
          ],
        ],
        '#element_validate' => [
          [$this, 'settingsFormRequiredValidate'],
        ],
      ];
    }

    /** @var \Drupal\media\Plugin\media\Source\OEmbedInterface $source */
    $source = $items->getEntity()->getSource();
    $providers = $source->getProviders();

    $description[] = $this->t('You can link to media from the following services: @providers', ['@providers' => implode(', ', $providers)]);

    if (count($providers) > 1 || !in_array('YouTube', $providers)) {
      $description[] = $this->t('Be aware that at the moment only YouTube videos can be submitted to Scribit Pro.');
    }

    if (!empty($element['#value']['#description'])) {
      $description[] = $element['#value']['#description'];
    }

    $element['value']['#description'] = count($description) > 1 ? [
      '#theme' => 'item_list',
      '#items' => $description,
    ] : reset($description);

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsFormRequiredValidate($element, FormStateInterface $form_state) {
    $scribit_pro_enabled = $element['#parents'];
    array_pop($scribit_pro_enabled);
    $scribit_pro_enabled[] = 'enable';
    $scribit_pro_enabled_value = $form_state->getValue($scribit_pro_enabled);
    $submitted_value = $form_state->getValue($element['#parents']);
    if ($scribit_pro_enabled_value && empty($submitted_value)) {
      $form_state->setError($element, $this->t('@name field is required.', ['@name' => $element['#title']]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $prepared_values = [];

    foreach ($values as $value_key => $item) {
      foreach ($item as $item_key => $item_value) {
        if ($item_key == 'scribit_pro') {
          if (isset($item_value['enable']) && $item_value['enable'] == 1 && !empty($item_value['voice']) && !empty($item_value['language']) && !$this->helper->getProjectByYouTubeUrl($item['value'])) {
            $post_video = $this->helper->postYoutubeVideo($item['value'], $item_value['voice'], $item_value['language']);

            if (isset($post_video['error']) && $post_video['error']) {
              \Drupal::messenger()->addWarning($post_video['body']->detail);
            }
            else {
              \Drupal::messenger()->addStatus('Video has been submitted to Scribit Pro');
            }
          }
        }
        else {
          $prepared_values[$value_key][$item_key] = $item_value;
        }
      }
    }

    return parent::massageFormValues($prepared_values, $form, $form_state);
  }

}
