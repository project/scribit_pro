<?php

namespace Drupal\scribit_pro;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueFactory;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\key\KeyRepositoryInterface;
use GuzzleHttp\ClientInterface;

/**
 * Class ApiService.
 *
 * This class handles all communication with Scribit Pro.
 */
class ApiService {
  use StringTranslationTrait;

  /**
   * Scribit Pro api base url.
   */
  private const BASE_URL = 'https://api.scribit.pro';

  /**
   * How long in seconds before expiration of the token it has to be renewed.
   *
   * Default is 1 hour.
   */
  private const EXPIRATION_OFFSET = 1 * 60 * 60;

  /**
   * Drupal\key\KeyRepositoryInterface definition.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * Drupal\Core\Config\Config definition.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * GuzzleHttp\ClientInterface definition.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Drupal\Core\KeyValueStore\KeyValueStoreInterface instance.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected $keyValue;

  /**
   * Drupal\Core\Session\AccountProxy definition.
   *
   * @var \Drupal\Core\Session\AccountProxy
   */
  protected $currentUser;

  /**
   * Logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * Messenger.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Constructs a new ApiService object.
   */
  public function __construct(KeyRepositoryInterface $key_repository, ConfigFactoryInterface $config_factory, ClientInterface $http_client, KeyValueFactory $key_value, AccountProxy $current_user, LoggerChannelFactoryInterface $logger_factory, MessengerInterface $messenger_interface) {
    $this->keyRepository = $key_repository;
    $this->config = $config_factory->getEditable('scribit_pro.config');
    $this->httpClient = $http_client;
    $this->keyValue = $key_value->get('scribit_pro');
    $this->currentUser = $current_user;
    $this->logger = $logger_factory->get('scribit_pro');
    $this->messenger = $messenger_interface;
  }

  /**
   * Get Scribit Pro projects.
   *
   * @return array
   *   Array containg projects.
   */
  public function getProjects() {
    return $this->authenticatedGetRequest('/v1/projects');
  }

  /**
   * Get project for given project id.
   *
   * @param string $project_id
   *   Project id.
   *
   * @return array
   *   Array containg the request response.
   */
  public function getProject($project_id) {
    return $this->authenticatedGetRequest('/v1/projects/' . $project_id);
  }

  /**
   * Get project by YouTube id.
   *
   * @param string $youtube_id
   *   YouTube video id.
   *
   * @return object|null
   *   Returns object if project exists for given YouTube video id. Returns null
   *   otherwise.
   */
  public function getProjectByYouTubeId($youtube_id) {
    $projects = $this->getProjects();
    if (!isset($projects['body'])) {
      return NULL;
    }
    foreach ($projects['body'] as $project) {
      foreach ($project->videos as $video) {
        if ($video->identifier == $youtube_id) {
          return $project;
        }
      }
    }

    return NULL;
  }

  /**
   * Post new video (project) to Scribit Pro.
   *
   * @param string $video_link
   *   Video url.
   * @param string $video_id
   *   Video id.
   * @param string $voice_name
   *   Voice name.
   * @param string $voice_language
   *   Voice language.
   *
   * @return array
   *   Array containing the request response.
   */
  public function postProject($video_link, $video_id, $voice_name, $voice_language) {
    $prepared_project = [
      'external_video_link' => $video_link,
      'external_video_id' => $video_id,
      'voice' => [
        'name' => $voice_name,
        'language' => $voice_language,
      ],
    ];
    return $this->authenticatedPostRequest('/v1/projects', ['Content-Type: application/json'], $prepared_project);
  }

  /**
   * Get Scribit Pro project media links for given project id.
   *
   * @param string $project_id
   *   Scribit Pro project id.
   *
   * @return object
   *   Object containg request body.
   */
  public function getProjectMediaLinks($project_id) {
    $request = $this->authenticatedGetRequest('/v1/projects/' . $project_id . '/requestMediaLinks');

    return $request['body'];
  }

  /**
   * Check if video exists for given project id.
   *
   * @param string $project_id
   *   Scribit Pro project id.
   *
   * @return array
   *   Array containging request response.
   */
  public function videoExists($project_id) {
    return $this->authenticatedGetRequest('/v1/projects/' . $project_id . '/videoExists');
  }

  /**
   * Sign in.
   *
   * @param string $email_key
   *   Email key.
   * @param string $password_key
   *   Password key.
   *
   * @return array
   *   Array containing the request response.
   */
  private function signIn($email_key = NULL, $password_key = NULL) {
    $headers = [];
    $headers[] = 'Authorization: Basic ' . $this->getEncodedAuthorizationCredentials($email_key, $password_key);

    return $this->postRequest('/v1/jwt/signin', $headers);
  }

  /**
   * Send a post request.
   *
   * @param string $uri
   *   Request uri.
   * @param array $headers
   *   Headers to be set for the request.
   * @param array $payload
   *   Request payload.
   *
   * @return array
   *   Array containing the request response.
   */
  private function postRequest($uri, array $headers, array $payload = []) {
    return $this->request('POST', $uri, $headers, $payload);
  }

  /**
   * Send an authenticated post request.
   *
   * @param string $uri
   *   Request uri.
   * @param array $headers
   *   Headers to be set for the request.
   * @param array $payload
   *   Request payload.
   *
   * @return array
   *   Array containing the request response.
   */
  private function authenticatedPostRequest($uri, array $headers = [], array $payload = []) {
    $token = $this->getBearerToken();
    $headers[] = 'X-auth: ' . rtrim($token['token']);

    return $this->postRequest($uri, $headers, $payload);
  }

  /**
   * Send a get request.
   *
   * @param string $uri
   *   Request uri.
   * @param array $headers
   *   Headers to be set for the request.
   * @param array $payload
   *   Request payload.
   *
   * @return array
   *   Array containing the request response.
   */
  private function getRequest($uri, array $headers, array $payload = []) {
    return $this->request('GET', $uri, $headers, $payload);
  }

  /**
   * Send an authenticated get request.
   *
   * @param string $uri
   *   Request uri.
   * @param array $headers
   *   Headers to be set for the request.
   * @param array $payload
   *   Request payload.
   *
   * @return array
   *   Array containing the request response.
   */
  private function authenticatedGetRequest($uri, array $headers = [], array $payload = []) {
    $token = $this->getBearerToken();
    if (!isset($token['token'])) {
      return NULL;
    }
    $headers[] = 'X-auth: ' . rtrim($token['token']);

    return $this->getRequest($uri, $headers, $payload);
  }

  /**
   * API Request.
   *
   * @param string $method
   *   Request method.
   * @param string $uri
   *   Uri for request.
   * @param array $headers
   *   Headers for the request.
   * @param array $payload
   *   (optional) Request payload.
   *
   * @return array
   *   Request response.
   */
  private function request($method, $uri, array $headers, array $payload = []) {
    $curl = curl_init();
    $curl_array = [
      CURLOPT_URL => $this::BASE_URL . $uri,
      CURLOPT_RETURNTRANSFER => TRUE,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => TRUE,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => $method,
      CURLOPT_HEADER => 1,
    ];

    $content_length =  0;
    if ($payload) {
      $curl_array[CURLOPT_POSTFIELDS] = json_encode($payload, JSON_UNESCAPED_SLASHES);
      $content_length = strlen(json_encode($payload, JSON_UNESCAPED_SLASHES));
    }
    $headers = array_merge(
      [
        'Accept: application/json, text/plain, */*',
        'Accept-encoding: gzip, deflate, br',
        'Content-Length: ' . $content_length,
      ],
      $headers
    );
    $curl_array[CURLOPT_HTTPHEADER] = $headers;

    curl_setopt_array($curl, $curl_array);

    $response = curl_exec($curl);
    $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    $header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
    $response_headers = explode("\n", substr($response, 0, $header_size));
    $body = substr($response, $header_size);

    $return = [
      'headers' => $response_headers,
      'body' => json_decode($body),
    ];
    if ($status !== 200) {
      $return['error'] = TRUE;
      $this->setStatusError($status, $return['body']);
    }

    return $return;
  }

  /**
   * Set error message based on status code.
   *
   * @param int $status
   *   Status code.
   * @param object $body
   *   Body of request response.
   */
  private function setStatusError(int $status, $body) {
    switch ($status) {
      case 401:
        $this->messenger->addWarning($this->t('Authentication for Scribit Pro has failed. Check your Scribit Pro login credentials and try again.'));
        break;

      case 400:
        $this->messenger->addWarning($this->t('Something went wrong: @error', ['@error' => $body->detail]));
        break;

      case 404:
        $this->messenger->addWarning($this->t('Currently the API service of Scribit Pro is offline. Please upload your video\'s manually to Scribit Pro before adding them to the website.'));
        break;

      default:
        $this->messenger->addWarning($this->t('Something went wrong connecting to Scribit Pro.'));
        break;
    }
  }

  /**
   * Refresh Bearer token.
   *
   * @return string|void
   *   Bearer token.
   */
  private function refreshBearerToken() {
    $refreshed_token = $this->getRequest('/v1/jwt/refresh', ['X-auth: ' . rtrim($this->getCurrentBearerToken()['token'])]);

    if (!isset($refreshed_token['error'])) {
      $new_token = $this->getBearerTokenfromHeaders($refreshed_token['headers']);
      return $this->setBearerToken($new_token);
    }
  }

  /**
   * Get Bearer token.
   *
   * If current token is set and not expired; return current token.
   * If the current token is expired; refresh token.
   * If no current token is set; get a new token.
   *
   * @return array
   *   Array containing token and expiration.
   */
  private function getBearerToken() {
    $current_token = $this->getCurrentBearerToken();

    // A token has been set and has not expired.
    if ($current_token && isset($current_token['token']) && $current_token['token'] && isset($current_token['expires']) && time() < $current_token['expires']) {

      // Token is about to expire.
      if (time() > $current_token['expires'] - $this::EXPIRATION_OFFSET) {
        return $this->refreshBearerToken();
      }
      // Token is not expired.
      return $current_token;
    }

    // We don't have a token so get a new one.
    return $this->getNewBearerToken();
  }

  /**
   * Get stored Bearer token.
   *
   * @return array
   *   Array containing the current token and expiration date.
   */
  private function getCurrentBearerToken() {
    return $this->keyValue->get('bearer');
  }

  /**
   * Sign in and get new Bearer token.
   *
   * @return array|null
   *   Array containing the token and expiration date.
   */
  private function getNewBearerToken($email_key = NULL, $password_key = NULL) {

    $sign_in = $this->signIn($email_key, $password_key);

    if (is_array($sign_in) && is_object($sign_in['body']) && isset($sign_in['body']->user) && is_object($sign_in['body']->user) && $user = $sign_in['body']->user) {
      $this->keyValue->set('scribit_pro_user', json_encode($user));
      $token = $this->getBearerTokenfromHeaders($sign_in['headers']);

      return [
        'token' => $token,
        'user' => $sign_in['body']->user,
      ];
    }

    return NULL;

  }

  /**
   * Set Bearer Token.
   *
   * Set and store token and expiration date.
   *
   * @param string $token
   *   String containing the encoded Bearer token.
   *
   * @return array
   *   Array containing the token and expiration date.
   */
  private function setBearerToken($token) {

    // Get token parts.
    $token_parts = explode('.', $token);

    // Replace characters.
    $data = str_replace('-', '+', $token_parts[1]);
    $data = str_replace('_', '/', $data);

    // Base64 decode.
    $base64_decoded = base64_decode($data);

    // Json decode.
    $json_decoded = json_decode($base64_decoded);

    $new_token = [
      'token' => $token,
      'expires' => $json_decoded->exp,
    ];

    $this->keyValue->set('bearer', $new_token);

    return $new_token;
  }

  /**
   * Extract Bearer token from returned headers.
   *
   * @param array $headers
   *   Array containing response headers.
   *
   * @return string
   *   Bearer token.
   */
  private function getBearerTokenfromHeaders(array $headers) {
    foreach ($headers as $header) {
      if (strtolower(substr($header, 0, 14)) == 'authorization:') {
        return substr($header, 15);
      }
    }
  }

  /**
   * Check if Scribit Pro credentials have been set.
   *
   * @return bool
   *   Indicating whether credentials have been set.
   */
  public function hasCredentials() {
    try {
      $email_key = $this->config->get('email');
      $password_key = $this->config->get('password');

      $email = $this->keyRepository->getKey($email_key);
      $password = $this->keyRepository->getKey($password_key);

      return ($email && $password) ? TRUE : FALSE;
    }
    catch (\Exception $e) {
      $this->logger->warning($this->t('Something went wrong: @error', ['@error' => $e->getMessage()]));

      return FALSE;
    }

  }

  /**
   * Get saved authorization credentials.
   *
   * @return string
   *   Encoded authorization credentials.
   */
  private function getEncodedAuthorizationCredentials($email_key = NULL, $password_key = NULL) {
    $email_key = $email_key ? $email_key : $this->config->get('email');
    $password_key = $password_key ? $password_key : $this->config->get('password');

    $email = $this->keyRepository->getKey($email_key)->getKeyValue();
    $password = $this->keyRepository->getKey($password_key)->getKeyValue();

    return base64_encode($email . ':' . $password);
  }

  /**
   * Get local Scribit Pro user.
   *
   * Get locally stored Scribit Pro user object.
   *
   * @return object
   *   Object containing the Scribit Pro user.
   */
  public function getLocalScribitProUser() {
    if ($scribit_pro_user = $this->keyValue->get('scribit_pro_user')) {
      return json_decode($scribit_pro_user);
    }
  }

  /**
   * Get local Scribit Pro user id.
   *
   * Get locally stored Scribit Pro user id.
   *
   * @return string
   *   Returns the locally stored Scribit Pro user id.
   */
  public function getLocalScribitProUserId() {
    $user = $this->getLocalScribitProUser();

    return $user->id;
  }

  /**
   * Test user login.
   *
   * @return object|void
   *   User object if login succeeded.
   */
  public function testUserLogin($email_key, $password_key) {
    if ($token = $this->getNewBearerToken($email_key, $password_key)) {
      $this->setBearerToken($token['token']);

      return $token['user'];
    }
  }

}
