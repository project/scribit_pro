<?php

namespace Drupal\scribit_pro\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\key\KeyRepositoryInterface;
use Drupal\scribit_pro\HelperService;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigForm.
 */
class ConfigForm extends ConfigFormBase {

  /**
   * Drupal\key\KeyRepositoryInterface definition.
   *
   * @var \Drupal\key\KeyRepositoryInterface
   */
  protected $keyRepository;

  /**
   * Scribit Pro helper.
   *
   * @var \Drupal\scribit_pro\HelperService
   */
  protected $helper;

  /**
   * Constructs a new ConfigForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\key\KeyRepositoryInterface $key_repository
   *   The key repository.
   * @param \Drupal\scribit_pro\HelperService $helper_service
   *   The Scribit Pro helper service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, KeyRepositoryInterface $key_repository, HelperService $helper_service) {
    parent::__construct($config_factory);

    $this->keyRepository = $key_repository;
    $this->helper = $helper_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('key.repository'),
      $container->get('scribit_pro.helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['scribit_pro.config'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('scribit_pro.config');

    if ($input = $form_state->getUserInput()) {
      $email = isset($input['email']) ? $input['email'] : $config->get('email');
      $password = isset($input['password']) ? $input['password'] : $config->get('password');
    }
    else {
      $email = $config->get('email');
      $password = $config->get('password');
    }

    if ($email && $password && $user = $this->helper->getScribitProUserLoginStatus($email, $password)) {
      $form['user_status'] = [
        '#type' => 'table',
        '#header' => [
          'username' => $this->t('Username'),
          'email' => $this->t('Email'),
        ],
        '#rows' => [
          [
            $user->username,
            $user->email,
          ],
        ],
      ];
    }
    else {
      $form['login_message'] = [
        '#type' => 'item',
        '#markup' => '<p>' . $this->t('Login using your Scribit Pro credentials.') . '</p>',
        '#weight' => -1,
      ];
    }

    $form['email'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Email'),
      '#description' => $this->t('Email'),
      '#weight' => '0',
      '#default_value' => $email ? $email : NULL,
      '#required' => TRUE,
    ];

    $form['password'] = [
      '#type' => 'key_select',
      '#title' => $this->t('Password'),
      '#description' => $this->t('Password'),
      '#weight' => '1',
      '#default_value' => $password ? $password : NULL,
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#weight' => '2',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();

    $this->config('scribit_pro.config')
      ->set('email', $values['email'])
      ->set('password', $values['password'])
      ->save();
  }

}
