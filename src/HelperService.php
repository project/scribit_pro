<?php

namespace Drupal\scribit_pro;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class HelperService.
 *
 * Helper service that provides additional functionality we don't want to
 * include in the api service.
 */
class HelperService {
  use StringTranslationTrait;

  /**
   * YouTube video url to be used for posting video's to Scribit Pro.
   */
  private const YOUTUBE_VIDEO_URL = 'https://www.youtube.com/watch?v=';

  /**
   * Drupal\scribit_pro\ApiService definition.
   *
   * @var \Drupal\scribit_pro\ApiService
   */
  protected $api;

  /**
   * Constructs a new HelperService object.
   */
  public function __construct(ApiService $scribit_pro_api) {
    $this->api = $scribit_pro_api;
  }

  /**
   * Get local Scribit Pro user.
   *
   * Get locally stored Scribit Pro user object.
   *
   * @return object
   *   Object containing the Scribit Pro user.
   */
  public function getLocalScribitProUser() {
    return $this->api->getLocalScribitProUser();
  }

  /**
   * Get local Scribit Pro user id.
   *
   * Get locally stored Scribit Pro user id.
   *
   * @return string
   *   Returns the locally stored Scribit Pro user id.
   */
  public function getLocalScribitProUserId() {
    return $this->api->getLocalScribitProUserId();
  }

  /**
   * Post youtube video to Scribit Pro.
   *
   * @param string $video_url
   *   YouTube video url.
   * @param string $voice_gender
   *   Voice gender (male or female).
   * @param string $voice_language
   *   Scribit Pro voice language.
   *
   * @return array
   *   Array containing response from post request.
   */
  public function postYoutubeVideo($video_url, $voice_gender, $voice_language) {
    if (!empty($video_url) && $video_id = $this->extractYouTubeIdFromUrl($video_url)) {
      return $this->api->postProject($this->prepareYouTubeVideoUrl($video_id), $video_id, $this->getVoiceKey($voice_language, $voice_gender), $voice_language);
    }
  }

  /**
   * Get Scribit Pro user logion status.
   *
   * Tests if the user can login with the known credentials.
   *
   * @param string $username_key
   *   Username key.
   * @param string $password_key
   *   Password key.
   *
   * @return object|void
   *   Returns user object if login succeeded.
   */
  public function getScribitProUserLoginStatus($username_key = NULL, $password_key = NULL) {
    return $this->api->testUserLogin($username_key, $password_key);
  }

  /**
   * Prepare YouTube video url.
   *
   * @param string $video_id
   *   YouTube Video id.
   *
   * @return string
   *   Returns complete YouTube video url for Scribit Pro.
   */
  public function prepareYouTubeVideoUrl($video_id) {
    return self::YOUTUBE_VIDEO_URL . $video_id;
  }

  /**
   * Get YouTube id from video url.
   *
   * @param string $url
   *   YouTube url the video id needs exctracted from.
   *
   * @return string|null
   *   YouTube id.
   */
  public function extractYouTubeIdFromUrl($url) {
    preg_match('/(?<=(?:v|i)=)[a-zA-Z0-9-]+(?=&)|(?<=(?:v|i)\/)[^&\n]+|(?<=embed\/)[^"&\n]+|(?<=(?:v|i)=)[^&\n]+|(?<=youtu.be\/)[^&\n]+/', $url, $id);

    return $id[0] ? $id[0] : NULL;
  }

  /**
   * Get available languages for Scribit Pro.
   *
   * @return array
   *   Array containing available Scribit Pro Languages.
   */
  public function getLanguages() {
    return [
      'en_uk' => $this->t('English (EN)'),
      'nl_nl' => $this->t('Dutch (NL)'),
      'nl_be' => $this->t('Flemish (BE)'),
      'sv_se' => $this->t('Swedish (SE)'),
    ];
  }

  /**
   * Get available voices for Scribit Pro.
   *
   * @return array
   *   Multi dimensional array containing available Scribit Pro Voices per
   *   available language code.
   */
  public function getVoices() {
    return [
      'nl_nl' => [
        'male' => 'Guus',
        'female' => 'Ilse',
      ],
      'en_uk' => [
        'male' => 'Hugh',
        'female' => 'Alice',
      ],
      'nl_be' => [
        'female' => 'Veerle',
      ],
      'sv_se' => [
        'male' => 'Sven',
        'female' => 'Karin',
      ],
    ];
  }

  /**
   * Get Voice key.
   *
   * Returns the voice key for the given language and gender.
   *
   * @param string $language
   *   Language code.
   * @param string $gender
   *   Gender (male or female).
   *
   * @return string
   *   Voice key for given language and gender.
   */
  public function getVoiceKey($language, $gender) {
    return $this->getVoices()[$language][$gender];
  }

  /**
   * Check if Scribit Pro credentials have been set.
   *
   * @return bool
   *   Indicating whether credentials have been set.
   */
  public function hasCredentials() {
    return $this->api->hasCredentials();
  }

  /**
   * Get project for given YouTube video url.
   *
   * @param string $youtube_url
   *   YouTube video url.
   *
   * @return object|null
   *   Returns project if found, otherwise null.
   */
  public function getProjectByYouTubeUrl($youtube_url) {
    if (!empty($youtube_url) && $youtube_id = $this->extractYouTubeIdFromUrl($youtube_url)) {
      return $this->api->getProjectByYouTubeId($youtube_id);
    }

    return NULL;
  }

}
