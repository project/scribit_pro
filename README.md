CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Scribit Pro module provides an easy way to implement the Scribit [Pro video
player embed](https://scribit.pro/en/diensten/youtube-widget/).

This module provides a media video formatter and widget.
This will enable you to submit video's to Scribit Pro.

 * For the description of the module visit:
   https://www.drupal.org/project/scribit_pro

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/scribit_pro


REQUIREMENTS
------------

 * [Display Suite](https://www.drupal.org/project/ds)
 * [Key](https://www.drupal.org/project/key)


INSTALLATION
------------

Install the Scribit Pro module as you would normally install a contributed
Drupal module. Visit https://www.drupal.org/node/1897420 for further
information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the Scribit Pro module.
    2. Navigate to Administration > Configuration > System > Keys and add keys
    for the Scribit Pro Email address and Password.
    3. Navigate to Administration > Configuration > System > Scribit Pro
    Configuration and select the keys for the Email and Password.

**Configuring new video fields (Go to the next section if you have existing
video fields you want to configure)**

    1. Navigate to Structure > Media Types
       1. Choose 'Add media type'
       2. For Media Source select 'Remote video' and click 'save'
    2. Continue by following the steps below.

**Configuring existing video fields (of type 'Remote Video')**

    1. Navigate to Administration > Structure > Media Types > [Your video media
    type] > Manage Display
    2. Set the widget for the Remote video URL to 'Scribit Pro oEmbed URL' and
    click 'save'
    3. Navigate to Administration > Structure > Media Types > [Your video media
    type] > Manage Form Display
    4. Set the Format for the Remote video URL to 'Scribit Pro formatter' and
    click 'save'


MAINTAINERS
-----------

Current maintainers:

 * [Tijmen van Eijsden (tijmenve)](https://www.drupal.org/u/tijmenve)

Supporting organization:

 * [Fonkel](https://www.drupal.org/fonkel)
