/**
 * @file
 * Attaches js file.
 */
(function ($, Drupal) {
  'use strict';
  Drupal.behaviors.scribitProFieldWidget = {
    attach: function (context, settings) {
      $('[data-scribit-pro-container="1"]').each(function() {
        var lang = $(this).find('[data-scribit-pro-field="language"]');
        var voice = $(this).find('[data-scribit-pro-field="voice"]');

        lang.on('change', function () {
          if (this.value == 'nl_be') {
            voice.filter('[value="male"]').attr('disabled', true);
            voice.filter('[value="female"]').prop('checked', true);
          }
          else if (this.value !== undefined)  {
            voice.filter('[disabled=disabled]').attr('disabled', false);
          }
        });
      });
    }
  };
})(jQuery, Drupal);
