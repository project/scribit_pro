/**
 * @file
 * Attaches js file.
 */
(function ($, Drupal, drupalSettings) {
  "use strict";
  Drupal.behaviors.scribitProEmbed = {
    attach: function (context, settings) {
      (function (s, w, i, d, g, e, t) {
        s["initScribitWidget"] = function () {
          s["scribitWidget"] = new s.scribit.widget(g);
        };
        e = w.createElement(i);
        e.type = "text/javascript";
        e.src = d;
        e.defer = true;
        t = w.getElementsByTagName(i)[0];
        t.parentNode.insertBefore(e, t);
      })(
        window,
        document,
        "script",
        "//widget.scribit.pro/main.js",
        drupalSettings.scribitPro.user_id
      );
    },
  };
})(jQuery, Drupal, drupalSettings);
